import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import HomeScreen from './Home';

const render = () => renderer.create(<HomeScreen navigation={this.mockNavigation} />);
describe('Home screen component', () => {
  this.mockNavigation = {};
  beforeEach(() => {
    this.mockNavigation.navigate = jest.fn();
  });

  it('renders correctly', () => {
    render();
  });

  it('navigates to LogForm', () => {
    const screen = render();
    screen.getInstance().onLogPressed();

    expect(this.mockNavigation.navigate).toBeCalledWith('LogForm');
  });
});
