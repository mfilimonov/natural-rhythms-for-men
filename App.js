import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './Home';
import LogForm from './LogForm';

const createComponent = (instance, props) =>
  navProps => React.createElement(instance, { ...props, ...navProps });


export default class NaturalRhythms extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    entries: [],
  }
  onLogSubmitted = (entry) => {
    this.setState({
      entries: this.state.entries.concat([entry]),
    });
  }
  render() {
    const Nav = StackNavigator({
      Home: {
        screen: createComponent(HomeScreen, { entries: this.state.entries }),
      },
      LogForm: {
        screen: createComponent(LogForm, { onLogSubmitted: this.onLogSubmitted }),
        navigationOptions: {
          title: 'Add New Log',
        },
      },
    });
    return (
      <Nav ref={(nav) => { this.navigator = nav; }} />
    );
  }
}
