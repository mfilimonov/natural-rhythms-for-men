import React, { Component } from 'react';
import {
  View,
  Slider,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';

import { translateSexualEnergy } from './translator';

const MyDatePicker = (props) => {
  const currentDate = moment().format();
  return (
    <View style={{ alignItems: 'center', padding: 10 }}>
      <Text style={styles.label}>Log Date</Text>
      <DatePicker
        style={{ width: '100%' }}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        mode="datetime"
        maxDate={currentDate}
        onDateChange={props.onDateChange}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    flex: 1,
    justifyContent: 'space-between',
  },
  sexualEnergyContainer: {
    alignItems: 'center',
  },
  label: {
    marginBottom: 10,
    fontSize: 25,
  },
  translation: {
    fontSize: 16,
    textDecorationLine: 'underline',
  },
  slider: {
    flex: 1,
    margin: 10,
  },
  logButtonContainer: {
    alignItems: 'center',
    borderWidth: 1,
    backgroundColor: '#8024ab',
  },
  logButton: {
    fontSize: 25,
    padding: 10,
    color: 'white',
  },
});

export default class LogForm extends Component {
  static navigationOptions = {
    title: 'Add New Log',
  }
  static propTypes = { onLogSubmitted: PropTypes.func.isRequired }
  state = {
    sexualEnergy: 5,
  }
  onSexualEnergyChange = (val) => {
    this.setState({
      sexualEnergy: val,
    });
  }
  onDateChange = (value) => {
    this.setState({ date: value });
  }
  onLogSubmitting = () => {
    const { sexualEnergy, date } = this.state;
    this.props.onLogSubmitted({ sexualEnergy, date });
    this.props.navigation.goBack();
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <View style={styles.sexualEnergyContainer}>
            <Text style={styles.label}>
              Choose Your Sexual Level
            </Text>
            <Text style={styles.translation}>
              {translateSexualEnergy(this.state.sexualEnergy)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
            <Text>
              1
            </Text>
            <Slider
              style={styles.slider}
              step={1}
              value={this.state.sexualEnergy}
              minimumValue={0}
              maximumValue={10}
              onValueChange={this.onSexualEnergyChange}
            />
            <Text>
              10
            </Text>
          </View>
          <MyDatePicker onDateChange={this.onDateChange} />
        </View>
        <TouchableHighlight style={styles.logButtonContainer} onPress={this.onLogSubmitting}>
          <Text style={styles.logButton}>Create new log</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
