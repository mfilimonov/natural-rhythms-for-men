import { translateSexualEnergy } from './translator';

describe('translateSexualEnergy', () => {
  it('returns a value for known sexual energy', () => {
    const output = translateSexualEnergy(2);

    expect(output.length).toBeGreaterThan(0);
  });
  it('returns default value for unknown sexual energy', () => {
    const output = translateSexualEnergy(10000);

    expect(output.length).toBeGreaterThan(0);
  });
});
