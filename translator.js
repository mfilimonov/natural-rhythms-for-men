import translationTable from './translation.json';

export function translateSexualEnergy(sexualEnergy) {
  const translation = translationTable.sexualEnergy[sexualEnergy];
  return translation || 'Huh... You have reached new limits';
}
