import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';
import moment from 'moment';

import App from './App';

const render = () => renderer.create(<App />);
describe('Main component', () => {
  it('renders correctly', () => {
    render();
  });
  it('it saves the navigator', () => {
    const tree = render();
    expect(tree.getInstance().navigator).toBeDefined();
  });
  it('adds entry on log submission', () => {
    const instance = render().getInstance();

    instance.onLogSubmitted({ sexualEnergy: '7', date: moment().format() });

    expect(instance.state.entries.length).toEqual(1);
  });
});
