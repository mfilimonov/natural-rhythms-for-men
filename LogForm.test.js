import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import React from 'react';

import { shallow } from 'enzyme';

import LogForm from './LogForm';

Enzyme.configure({ adapter: new Adapter() });
const render = (props) => {
  const defaultProps = { onLogSubmitted: jest.fn(), navigation: { goBack: jest.fn() } };
  return shallow(React.createElement(LogForm, { ...defaultProps, ...props }));
};

const moveSlider = (value, component) => {
  component.find('Slider').simulate('valueChange', 2);
  return component;
};
const changeDate = (value, component) => {
  component.find('MyDatePicker').simulate('dateChange', value);
  return component;
};
const submit = (component) => {
  component.find('TouchableHighlight').simulate('press');
  return component;
};
describe('LogForm', () => {
  it('renders correctly', () => {
    render();
  });
  it('changes sexual on change of slider value', () => {
    const component = render();

    moveSlider(2, component);

    expect(component).toMatchSnapshot();
  });
  it('calls callback on submit', () => {
    const logSubmitted = jest.fn();
    const view = render({ onLogSubmitted: logSubmitted });

    moveSlider(2, view);
    changeDate('2017-01-01', view);
    submit(view);

    expect(logSubmitted).toBeCalledWith({
      sexualEnergy: 2,
      date: '2017-01-01',
    });
  });
  it('goes back on submit', () => {
    const navMock = {
      goBack: jest.fn(),
    };
    const view = render({ navigation: navMock });

    submit(view);

    expect(navMock.goBack).toBeCalled();
  });
});
