import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  topNav: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    backgroundColor: 'lightgray',
  },
  headerText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  logButtonContainer: {
    alignItems: 'center',
    borderWidth: 1,
    margin: 7,
    backgroundColor: '#8024ab',
  },
  logButton: {
    fontSize: 30,
    padding: 10,
    color: 'white',
  },
});

export default class HomeScreen extends Component {
  static propTypes = {
    navigation: PropTypes.shape({ navigate: PropTypes.func.isRequired }).isRequired,
    entries: PropTypes.arrayOf(PropTypes.shape({
      sexualEnergy: PropTypes.Number,
    })),
  }
  static defaultProps = {
    entries: [],
  }
  onLogPressed =() => {
    this.props.navigation.navigate('LogForm');
  }
  renderEntries = () =>
    this.props.entries.map(entry => (<Text
      style={{
 padding: 10, margin: 5, borderWidth: 1, borderColor: 'gray',
}}
      key={entry.date}
    >
      Energy Level {entry.sexualEnergy} {moment(entry.date).fromNow()}
    </Text>))
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topNav}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.headerText}>
          33
            </Text>
            <Text>Day Of Streak</Text>
          </View>
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.headerText}>
          High
            </Text>
            <Text>Average Energy</Text>
          </View>
        </View>
        <View>
          {this.renderEntries()}
        </View>
        <TouchableHighlight style={styles.logButtonContainer} onPress={this.onLogPressed}>
          <Text style={styles.logButton}>Log</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
